package edu.iastate.cpre388.lecture17_reusinglayouts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Backing1Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);

        // Customize the layout to let the user know which Activity is active.
        TextView tv = findViewById(R.id.infoTextView);
        tv.setText(R.string.backing1_info);
    }

    public void onClick(View v) {
        // Show a toast letting the user know what Activity is backing the shared layout.
        Toast.makeText(this, "Backing1", Toast.LENGTH_SHORT).show();

        // Using an explicit intent, launch Backing2Activity, which happens to use a new instance
        // of the same Layout used in this Activity.
        Intent explicitIntent = new Intent(this, Backing2Activity.class);
        startActivity(explicitIntent);
    }
}

package edu.iastate.cpre388.lecture17_reusinglayouts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Backing2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);

        // Customize the shared layout with a string in the TextView.
        TextView tv = findViewById(R.id.infoTextView);
        tv.setText(R.string.backing2_info);
    }

    // This event handler is defined in the Layout.  Notice that it has the same signature as in
    // Backing1Activity.
    public void onClick(View v) {
        // Show a toast letting the user know what Activity is backing the shared layout.
        Toast.makeText(this, "Backing2", Toast.LENGTH_SHORT).show();
    }
}

package edu.iastate.cpre388.lecture17_implicitintent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onWebClick(View v) {
        Intent implicitIntent = new Intent();
        implicitIntent.setAction(Intent.ACTION_VIEW);
        implicitIntent.setData(Uri.parse("https://iastate.edu"));
//        implicitIntent.setData(Uri.parse("unknownuri://iastate.edu"));
        // Check that the intent can be resolved to avoid a runtime exception on startActvitiy().
        if (implicitIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(implicitIntent);
        } else {
            Toast.makeText(this, "No available app", Toast.LENGTH_SHORT).show();
        }
    }
}
